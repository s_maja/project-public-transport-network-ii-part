package UndoRedo;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import GUI.LeftMenuGUI;
import graph.Graph;
import graph.Node;

public class AddNode extends AbstractUndoableEdit {

	private Graph g;
	private LeftMenuGUI leftMenu;
	private String s;
	private double x,y;
	
	public AddNode(Graph g, LeftMenuGUI leftMenu, double x, double y) {
		super();
		this.g = g;
		this.leftMenu = leftMenu;
		s=leftMenu.getNewNode();
	}

	@Override
	public void redo() throws CannotRedoException {
		Node n= new Node(s);
		g.addNode(n);
		n.moveXY(x,y);
		leftMenu.setBtnAdd(true);
	}
	
	@Override
	public void undo() throws CannotUndoException {
		g.removeNode(g.getNodes().get(s));
		leftMenu.setBtnAdd(false);
	}
}
