package UndoRedo;

import java.util.HashMap;

import javax.swing.plaf.SliderUI;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import graph.Graph;
import javafx.scene.text.Text;
import javafx.scene.control.Slider;

public class ChangeNodeSize extends AbstractUndoableEdit {
	private Graph g;
	private Slider slider;
	private Text txt;
	private HashMap<graph.Node, Integer> oldValues = new HashMap<graph.Node, Integer>();
	double newSize;
	
	public ChangeNodeSize(Graph g, Slider slider, Text txt) {
		super();
		this.g = g;
		this.slider = slider;
		this.txt = txt;
		newSize=slider.getValue();
		for(graph.Node n: g.getNodes().values()) {
			oldValues.put(n, (int) n.getSize());
		}
	}
	
	@Override
	public void undo() throws CannotUndoException {
		for(graph.Node n: g.getNodes().values()) {
			n.setRadius(oldValues.get(n));
			txt.setText("Size  " + (int)oldValues.get(n));
		}
	}

	@Override
	public void redo() throws CannotRedoException {
		for(graph.Node n: g.getNodes().values()) {
			n.setRadius(newSize);
			txt.setText("Size  " + (int)newSize);
		}
	}

}

