package UndoRedo;

import java.util.HashMap;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.*;

public class ColorNode extends AbstractUndoableEdit {

	private ColorPicker cp;
	private HashMap<graph.Node, Color> oldValues= new HashMap<graph.Node, Color>();
	private Color newColor;
	
	public ColorNode(ColorPicker cp) {
		this.cp = cp;
		for(graph.Node n: graph.Node.getSelection()) {
			oldValues.put(n, (Color) n.getColor());
		}
		newColor=cp.getValue();
	}
	
	@Override
	public void redo() throws CannotRedoException {
		for(graph.Node n: oldValues.keySet()) {
			n.setColor(newColor);
			n.setFill(newColor);
			n.setStroke(newColor.brighter().brighter().brighter());
		}
	}
	
	@Override
	public void undo() throws CannotUndoException {
		for(graph.Node n: oldValues.keySet()) {
			n.setColor(oldValues.get(n));
			n.setFill(oldValues.get(n));
			n.setStroke(oldValues.get(n).brighter().brighter().brighter());
		}
	}
	
	
}
