package UndoRedo;
import javafx.scene.text.Text;
import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;

import graph.Edge;
import graph.Graph;
import graph.Node;
import graph.Pair;

public class AddEdge extends AbstractUndoableEdit {

	private Node n1, n2;
	private boolean i=true;
	private Graph g;

	public AddEdge(Graph g) {
		super();
		this.g = g;
		for(graph.Node n: graph.Node.getSelection()) {
			if(i)  n1=n; else n2=n;
			i=false;
		}
	}
	
	@Override
	public void undo() throws CannotUndoException {
		g.removeEdge(g.getEdges().get(new Pair(n1.ID(),n2.ID())));
	}
	
	@Override
	public void redo() throws CannotRedoException {
		String label= "Edge " + n1.ID() + "-" + n2.ID();
		g.addEdge(new Edge(n1,n2,label));
	}
	
}
