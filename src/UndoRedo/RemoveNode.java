package UndoRedo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javafx.scene.paint.*;
import graph.Edge;
import graph.Graph;
import graph.Node;
import graph.Pair;
import javafx.scene.text.Text;

public class RemoveNode extends AbstractUndoableEdit {
	
	private Graph g;
	private ArrayList<Node> selected= new ArrayList<Node>();
	private ArrayList<String> oldNode= new ArrayList<String>();
	private ArrayList<Pair<String,String>> oldEdges= new ArrayList<Pair<String,String>>(); 
	private HashMap<String,Double> oldX= new HashMap<String,Double>();
	private HashMap<String,Double> oldY= new HashMap<String,Double>();
	private HashMap<String,Color> oldColor= new HashMap<String,Color>();
	private HashMap<String,Text> oldLabel= new HashMap<String,Text>();
	private HashMap<Pair<String,String>, Text> labelsEdge = new HashMap<Pair<String,String>, Text> ();
	
	public RemoveNode(Graph g) {
		this.g=g;
		for(graph.Node n: graph.Node.getSelection()) {
			oldNode.add(n.ID());
			selected.add(n);
			oldX.put(n.ID(),n.getCenterX());
			oldY.put(n.ID(),n.getCenterY());
			oldColor.put(n.ID(),n.getColor());
			oldLabel.put(n.ID(), n.getText());
			for(Edge e: n.getMyEdges().values()) {
				if((oldEdges.contains(new Pair(e.getNode1().ID(),e.getNode2().ID())))||
						(oldEdges.contains(new Pair(e.getNode2().ID(),e.getNode1().ID())))) continue;
				oldEdges.add(new Pair(e.getNode1().ID(),e.getNode2().ID()));
				labelsEdge.put(new Pair(e.getNode1().ID(),e.getNode2().ID()), e.getText());
			}
		}
	}
	
	@Override
	public void redo() throws CannotRedoException {
		for(graph.Node n: selected) {
			g.removeNode(n);
		}
	}
	
	@Override
	public void undo() throws CannotUndoException {
		for(String n: oldNode) {
			g.addNode(new Node(n));
			g.getNodes().get(n).moveXY(oldX.get(n),oldY.get(n));
			g.getNodes().get(n).setColor(oldColor.get(n));
			g.getNodes().get(n).setFill(oldColor.get(n));
			g.getNodes().get(n).setStroke(oldColor.get(n).brighter().brighter().brighter());
			g.getNodes().get(n).setText(oldLabel.get(n));
		}
		
		for(Pair<String,String> p : oldEdges) {
			String label= "Edge"+ p.getS1() + "-" + p.getS2();
			g.addEdge(new Edge(g.getNodes().get(p.getS1()),g.getNodes().get(p.getS2()) ,label));
			g.getEdges().get(new Pair(p.getS1(),p.getS2())).setText(labelsEdge.get(new Pair(p.getS1(),p.getS2())));
		}
		
	}
}
