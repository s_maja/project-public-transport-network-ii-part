package UndoRedo;

import javax.swing.undo.AbstractUndoableEdit;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javafx.scene.paint.*;
import graph.Edge;
import graph.Graph;
import graph.Node;
import graph.Pair;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class RemoveEdge extends AbstractUndoableEdit {
	private Graph g;
	private Node n1, n2;
	private Color oldColor;
	private String oldLabel;
	private Color labelColor;
	private double labelSize;
	
	public RemoveEdge(Graph g, Node n1, Node n2) {
		super();
		this.g = g;
		this.n1 = n1;
		this.n2 = n2;
		Edge e;
		if(g.getEdges().containsKey(new Pair(n1.getLabel(),n2.getLabel())))
			 e=g.getEdges().get(new Pair(n1.getLabel(),n2.getLabel()));
		else 
			 e=g.getEdges().get(new Pair(n2.getLabel(),n1.getLabel()));
		oldColor=e.getColor();
		oldLabel=e.getLabel();
		labelColor=(Color) e.getText().getFill();
		labelSize=e.getText().getFont().getSize();
			
	}

	@Override
	public void undo() throws CannotUndoException {
		g.addEdge(new Edge(n1, n2, oldLabel));
		g.getEdges().get(new Pair(n1.ID(),n2.ID())).setColor(oldColor);
		g.getEdges().get(new Pair(n1.ID(),n2.ID())).setStroke(oldColor);
		g.getEdges().get(new Pair(n1.ID(),n2.ID())).getText().setFill(labelColor);
		g.getEdges().get(new Pair(n1.ID(),n2.ID())).getText().setFont(Font.font(labelSize));
	}

	@Override
	public void redo() throws CannotRedoException {
		if(g.getEdges().containsKey(new Pair(n1.getLabel(),n2.getLabel()))) 
			g.removeEdge(g.getEdges().get(new Pair(n1.getLabel(),n2.getLabel())));
		else 
			g.removeEdge(g.getEdges().get(new Pair(n2.getLabel(),n1.getLabel())));
	}

}
