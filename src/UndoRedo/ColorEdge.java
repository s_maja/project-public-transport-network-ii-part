package UndoRedo;

import javax.swing.undo.AbstractUndoableEdit;

import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import graph.Edge;
import graph.Graph;
import graph.Node;
import graph.Pair;
import javafx.scene.control.ColorPicker;
import javafx.scene.paint.*;

public class ColorEdge extends AbstractUndoableEdit {
	private ColorPicker cp;
	private Graph g;
	private HashMap<Edge, Color> oldValue= new  HashMap<Edge, Color>();
	private Color newColor;
	private ArrayList<Node> selected = new ArrayList<Node>();
	
	public ColorEdge(ColorPicker cp, Graph g) {
		this.cp = cp;
		this.g=g;
		newColor=cp.getValue();
		for(graph.Node n1: graph.Node.getSelection()) {
			selected.add(n1);
		}
		
		for(graph.Node n1: graph.Node.getSelection()) {
			for(graph.Node n2: graph.Node.getSelection()) {
				if(n1==n2) continue;
				Edge e=g.getEdges().get(new Pair(n1.ID(),n2.ID()));
				if(e!=null) oldValue.put(e, e.getColor());
			}
		}
	}
	
	@Override
	public void undo() throws CannotUndoException {
		for(Edge e: oldValue.keySet()) {
			e.setColor(oldValue.get(e));
			e.setStroke(oldValue.get(e));
		}
	}
	
	@Override
	public void redo() throws CannotRedoException {
		for(graph.Node n1: selected) {
			for(graph.Node n2: selected) {
				if(n1==n2) continue;
				Edge e=g.getEdges().get(new Pair(n1.ID(),n2.ID()));
				if(e!=null)e.setColor(newColor);
				if(e!=null)e.setStroke(newColor);
			}
		}
	}
}
