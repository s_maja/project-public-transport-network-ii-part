package UndoRedo;

import javax.swing.undo.AbstractUndoableEdit;
import graph.Graph;
import graph.Node;
import graph.Pair;
import javafx.scene.text.Font;
import javafx.scene.control.*;
import javafx.scene.paint.*;

public class ChangeLabel extends AbstractUndoableEdit  {

	private Graph g;
	private boolean i;
	private Node n1, n2;
	private TextField newLabel;
	private  TextField sizeLabel;
	private ColorPicker cp;
	private String oldName; 
	private double oldSize;
	private Color oldColor;
	
	public ChangeLabel(Graph g, boolean i, Node n1, Node n2, TextField newLabel, TextField sizeLabel,ColorPicker cp ) {
		this.g=g;
		this.i=i;
		this.n1=n1;
		this.n2=n2;
		this.newLabel=newLabel;
		this.sizeLabel=sizeLabel;
		this.cp=cp;
		
		for(graph.Node n : graph.Node.getSelection()) {
			if(i)this.n1=n; else this.n2=n;
			i=false;
		}
		if(n2==null) {
			oldName=this.n1.getLabel();
			oldSize=this.n1.getText().getFont().getSize();
			oldColor=(Color) this.n1.getText().getFill();
		}
		else
		{
			oldName=g.getEdges().get(new Pair(this.n1.ID(), this.n2.ID())).getLabel();
			oldSize=g.getEdges().get(new Pair(this.n1.ID(), this.n2.ID())).getText().getFont().getSize();
			oldColor=(Color)g.getEdges().get(new Pair(this.n1.ID(), this.n2.ID())).getText().getFill();
		}
	}
	
	public void undo() {
		if(n2==null) {
			if(n1==null) System.out.println();
			n1.getText().setFont(Font.font(oldSize));
			n1.getText().setFill(oldColor);
			n1.setLabel(oldName);
		}else {
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).setLabel(oldName);
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFont(Font.font(oldSize));
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFill(oldColor);
		}
	}
	
	public void redo() {
	
		if( n2==null) {
			n1.getText().setFont(Font.font(Double.parseDouble(sizeLabel.getText())));
			n1.getText().setFill(cp.getValue());
			n1.setLabel(newLabel.getText());
		}
		else {
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).setLabel(newLabel.getText());
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFont(Font.font(Double.parseDouble(sizeLabel.getText())));
			g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFill(cp.getValue());
		}
	}
}
