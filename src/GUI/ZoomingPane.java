package GUI;

import javafx.scene.layout.Pane;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.transform.Scale;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;


public class ZoomingPane extends Pane{
	private static DoubleProperty increment= new SimpleDoubleProperty(0.5);
	private DoubleProperty zoomFactor = new SimpleDoubleProperty(1);
	private Pane content;
	
	
	public  ZoomingPane(Pane content) {
        this.content = content;
        getChildren().add(content);
        Scale scale = new Scale(1, 1);
        content.getTransforms().add(scale);
        
        zoomFactor.addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                scale.setX(newValue.doubleValue());
                scale.setY(newValue.doubleValue());
                requestLayout();
            }
        });
	}
	
	public static DoubleProperty getInc() {
		return  increment;
	}
	
	public static void incInc() {
		 increment.set(increment.doubleValue() + 0.1);
	}
	
	public static void decInc() {
		 increment.set(increment.doubleValue() - 0.1);
	}
	
        
    public final Double getZoomFactor() {
       return zoomFactor.get();
    }
    
    public final void setZoomFactor(Double zoomFactor) {
        this.zoomFactor.set(zoomFactor);
    }
    
    public final DoubleProperty zoomFactorProperty() {
            return zoomFactor;
    }
   
}
