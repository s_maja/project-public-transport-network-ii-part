package GUI;

import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.input.*;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.plaf.FileChooserUI;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import algorithms.ExportImport;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.control.ColorPicker;
import graph.*;

public class SideMenuGUI {
	private Stage stage;
	private Scene scene;
	private Pane p;
	private ZoomingPane zoomingPane;
	private ExportImport exportImport;
	private Graph g;
	private ColorPicker colorPicker1 = new ColorPicker();;
	private Boolean boo=true;
	private FileChooser fcWrite;
	private UndoManager um;
	
	public SideMenuGUI(Stage stage, Scene scene, Pane p, Graph g, ExportImport exportImport,FileChooser fcWrite, UndoManager um) {
		this.stage=stage;
		this.scene=scene;
		this.p=p;
		this.g=g;
		zoomingPane = new ZoomingPane(p);
		this.exportImport=  exportImport;;
		zoomingPane.zoomFactorProperty().bind(ZoomingPane.getInc());
		colorPicker1.setVisible(false);
		this.fcWrite=fcWrite;
		this.um=um;
	}
	
	public ZoomingPane getZoom() {
		return zoomingPane;
	}
	
	public ColorPicker getPicker() {
		return colorPicker1;
	}

	public VBox load() {
		VBox vbox= new VBox();
		vbox.setPrefWidth(80);
		for(int i=1;i<8;i++)
			vbox.getChildren().add(item(String.valueOf(i)));
		vbox.setStyle("-fx-background-color:#7a7979");
		return vbox;
	}
	
	private HBox item(String icon) {
		Image image= new Image(SideMenuGUI.class.getResource("/icon/icon"+icon+ ".png").toExternalForm());
		ImageView imageView= new ImageView(image);
		Button btn= new Button();
		btn.setGraphic(imageView);
		btn.setPrefSize(80, 140);
		btn.setStyle("-fx-background-color:#7a7979");
		Pane paneIndicator= new Pane();
		paneIndicator.setPrefSize(15, 140);
		paneIndicator.setStyle("-fx-background-color:#7a7979");
		action(btn,paneIndicator);
		HBox hbox= new HBox(paneIndicator,btn);
		switch(icon) {
		case "1":btn.setOnAction(t->{
			fcWrite= new FileChooser();
			FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
			fcWrite.getExtensionFilters().add(extFilter);
			File file = fcWrite.showSaveDialog(stage);
			if(file != null){
				exportImport.exporting(file);
            }else 
            	return;

			Alert alert = new Alert(AlertType.INFORMATION);
       	 	alert.setTitle("Information Dialog");
       	 	alert.setHeaderText(null);
       	 	alert.setContentText("Successful!");
       	 	alert.initOwner(stage);
       	 	alert.showAndWait();
		});
			break;
		case "2": btn.setOnAction(t->{
		        	 SnapshotParameters snapshotParameters = new SnapshotParameters();
		        	 snapshotParameters.setViewport(new Rectangle2D(0.0, 0.0 , scene.getWidth(), scene.getHeight()));
		        	 WritableImage wim = p.snapshot(snapshotParameters, null);
		        	 File file = new File("Image.png");
		        	 try {
		                 ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
		             } catch (Exception s) {}
		        	 Alert alert = new Alert(AlertType.INFORMATION);
		        	 alert.setTitle("Information Dialog");
		        	 alert.setHeaderText(null);
		        	 alert.setContentText("Successful!");
		        	 alert.initOwner(stage);
		        	 alert.showAndWait();
			});
			break;
		case "3":	
			paneIndicator.getChildren().add(colorPicker1);
			colorPicker1.toFront();
			btn.setOnAction(t->{
				colorPicker1.setVisible(boo);
				boo= !boo;	
				colorPicker1.relocate(paneIndicator.getWidth()-158, paneIndicator.getHeight()/2-66);
			});
			break;
		case "4":
			btn.setOnAction(event->{
				try {
		        	um.undo();
		        }catch(CannotUndoException e) {}
			});
			break;
		case "5":
			btn.setOnAction(event->{
				try {
		        	um.redo();
		        }catch(CannotRedoException e) {}
			});
			break;
		case "6":	//zoomIn
			btn.setOnAction(event->{
		        ZoomingPane.incInc();
			});
			break;
		case "7":	//zoomOut
			btn.setOnAction(event->{
				 ZoomingPane.decInc();	
			});
			break;
		}
		return hbox;
	}
	
	private void action(Button btn, Pane pane) {
		btn.setOnMouseEntered(t->{
			btn.setStyle("-fx-background-color:#545454");
			pane.setStyle("-fx-background-color:#5278b2");
		});
		
		btn.setOnMouseExited(t->{
			btn.setStyle("-fx-background-color:#7a7979");
			pane.setStyle("-fx-background-color:#7a7979");
		});
	}
	public void setGraph(Graph g) {
		this.g=g;
		exportImport.setG(g);      
	}
	
	
}
