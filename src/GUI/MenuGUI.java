package GUI;

import java.io.File;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.plaf.FileChooserUI;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import algorithms.ExportImport;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.WritableImage;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.Pane;
import graph.*;

public class MenuGUI {
	
	private Stage myStage;
	private Scene scene;
	private Pane p;
	private Graph g;
	private MenuItem item;
	private ExportImport exportImport;
	private ZoomingPane zoomPane;
	private FileChooser fcWrite;
	private UndoManager um;
	
	public MenuGUI(Stage myStage, Scene scene,Pane p, Graph g, ExportImport e,ZoomingPane z,FileChooser fcWrite, UndoManager undoMan) {
		this.myStage=myStage;
		this.scene=scene;
		this.p=p;
		this.g=g;
		exportImport=e;
		zoomPane=z;
		this.fcWrite=fcWrite;
		um=undoMan;
	}
	
	public MenuBar load() {
		MenuBar menuBar = new MenuBar();
		Menu menu1 = new Menu("File");
		Menu menu2= new Menu("Options");
		Menu menu3 = new Menu("View");
		Menu menu4 = new Menu("Help");
		
		menuBar.getMenus().add(menu1);
		menuBar.getMenus().add(menu2);
		menuBar.getMenus().add(menu3);
		menuBar.getMenus().add(menu4);
	
		MenuItem menuItem1 = new MenuItem("About");
		MenuItem menuItem2 = new MenuItem("Author");
		menu4.getItems().add(menuItem1);
		menu4.getItems().add(menuItem2);
		MenuItem menuItem3 = new MenuItem("Full Screen");
		menu3.getItems().add(menuItem3);
		
		MenuItem menuItem4 = new MenuItem("Undo");
		MenuItem menuItem5 = new MenuItem("Redo");
		MenuItem menuItem6 = new MenuItem("Zoom in");
		MenuItem menuItem7 = new MenuItem("Zoom out");
		menu2.getItems().add(menuItem4);
		menu2.getItems().add(menuItem5);
		menu2.getItems().add(menuItem6);
		menu2.getItems().add(menuItem7);
		
		MenuItem menuItem8 = new MenuItem("Open");
		MenuItem menuItem9 = new MenuItem("Export Image");
		MenuItem menuItem10 = new MenuItem("Save As");
		MenuItem menuItem11 = new MenuItem("Exit");
		item=menuItem8;
		menu1.getItems().add(menuItem8);
		menu1.getItems().add(menuItem9);
		menu1.getItems().add(menuItem10);
		menu1.getItems().add(menuItem11);
		
		
		
		menuItem3.setOnAction(new EventHandler<ActionEvent>() {  //FullScreen
		    @Override public void handle(ActionEvent e) {
		        myStage.setFullScreen(true);
		    }
		});
		
		menuItem11.setOnAction(new EventHandler<ActionEvent>() {  //Exit
		    @Override public void handle(ActionEvent e) {
		    	Alert alert = 
				        new Alert(AlertType.INFORMATION, "Do you want to save file?",
				             ButtonType.OK, 
				             ButtonType.CANCEL);
				alert.setTitle("Date format warning");
				Optional<ButtonType> result = alert.showAndWait();

				if (result.get() == ButtonType.OK){
					fcWrite= new FileChooser();
					FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
					fcWrite.getExtensionFilters().add(extFilter);
					File file = fcWrite.showSaveDialog(myStage);
					if(file != null){
						exportImport.exporting(file);
		            }else 
		            	return;
				} else {
					 Platform.exit();
				}
		    }
		});
		
		menuItem9.setOnAction(new EventHandler<ActionEvent>() { //Export Image
	        @Override public void handle(ActionEvent event) {
	        	 SnapshotParameters snapshotParameters = new SnapshotParameters();
	        	 snapshotParameters.setViewport(new Rectangle2D(0.0, 0.0 , scene.getWidth(), scene.getHeight()));
	        	 WritableImage wim = p.snapshot(snapshotParameters, null);
	        	 File file = new File("Image.png");
	        	 try {
	                 ImageIO.write(SwingFXUtils.fromFXImage(wim, null), "png", file);
	             } catch (Exception s) {}
	        	 Alert alert = new Alert(AlertType.INFORMATION);
	        	 alert.setTitle("Information Dialog");
	        	 alert.setHeaderText(null);
	        	 alert.setContentText("Successful!");
	        	 alert.initOwner(myStage);
	        	 alert.showAndWait();
	        }
	    });
		
		menuItem2.setOnAction(new EventHandler<ActionEvent>() { //Author
	        @Override public void handle(ActionEvent event) {
	        	Alert alert = new Alert(AlertType.INFORMATION);
	        	alert.setTitle("Author");
	        	alert.setHeaderText(null);
	        	alert.setContentText("Maja Skoko\n July 2018");
	        	alert.initOwner(myStage);
	        	alert.showAndWait();
	        }
	    });
		
		menuItem1.setOnAction(new EventHandler<ActionEvent>() { //About
	        @Override public void handle(ActionEvent event) {
	        	Alert alert = new Alert(AlertType.INFORMATION);
	        	alert.setTitle("About");
	        	alert.setHeaderText(null);
	        	alert.setContentText("This is network analysis and visualization software package written in Java on the Eclipse platform.");
	        	alert.initOwner(myStage);
	        	alert.showAndWait();
	        }
	    });
		
		menuItem6.setOnAction(new EventHandler<ActionEvent>() { //ZoomIN
	        @Override public void handle(ActionEvent event) {
	        	zoomPane.incInc();
	        }
		});
		
		menuItem7.setOnAction(new EventHandler<ActionEvent>() { //ZoomOUT
	        @Override public void handle(ActionEvent event) {
	        	zoomPane.decInc();
	        }
		});
		
		menuItem4.setOnAction(new EventHandler<ActionEvent>() { //Undo
	        @Override public void handle(ActionEvent event) {
	        	try {
	        	um.undo();
	        	}catch(CannotUndoException e) {}
	        }
		});
		
		menuItem5.setOnAction(new EventHandler<ActionEvent>() { //Redo
	        @Override public void handle(ActionEvent event) {
	        	try {
		        	um.redo();
		        }catch(CannotRedoException e) {}
	        }
		});
		
		menuItem10.setOnAction(new EventHandler<ActionEvent>() { //SaveAs
	        @Override public void handle(ActionEvent event) {
	        	fcWrite= new FileChooser();
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
				fcWrite.getExtensionFilters().add(extFilter);
				File file = fcWrite.showSaveDialog(myStage);
				if(file != null){
					exportImport.exporting(file);
	            }else 
	            	return;

				Alert alert = new Alert(AlertType.INFORMATION);
	       	 	alert.setTitle("Information Dialog");
	       	 	alert.setHeaderText(null);
	       	 	alert.setContentText("Successful!");
	       	 	alert.initOwner(myStage);
	       	 	alert.showAndWait();
	        }
		});
		return menuBar;
	}
	
	public MenuItem getItem() {
		return item;
	}
	
	public void setGraph(Graph g) {
		this.g=g;
	}
}
