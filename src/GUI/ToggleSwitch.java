package GUI;
import graph.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import javafx.scene.shape.Circle;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.layout.Pane;
import javafx.animation.TranslateTransition;
import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;

public class ToggleSwitch extends Pane {
	
	private Graph g;
	private SimpleBooleanProperty switchedOn= new SimpleBooleanProperty(false);
	private TranslateTransition translateAnimation= new TranslateTransition(Duration.seconds(0.25));
	private FillTransition fillAnimation= new FillTransition(Duration.seconds(0.25));
	private ParallelTransition animation= new ParallelTransition(translateAnimation,fillAnimation);

	
	public SimpleBooleanProperty switchedOnProperty() {
		return switchedOn;
	}
	
	public ToggleSwitch(Graph g) {
		this.g=g;
		Rectangle rect= new Rectangle(100,50);
		rect.setArcWidth(50);
		rect.setArcHeight(50);
		rect.setFill(Color.WHITE);
		rect.setStroke(Color.LIGHTGRAY);
		
		Circle trigger= new Circle(25);
		trigger.setCenterX(25);
		trigger.setCenterY(25);
		trigger.setFill(Color.WHITE);
		trigger.setStroke(Color.LIGHTGRAY);
		
		translateAnimation.setNode(trigger);
		fillAnimation.setShape(rect);
		getChildren().addAll(rect, trigger);
		
		switchedOn.addListener((obc, oldState, newState)->{
			boolean isOn= oldState.booleanValue();
			translateAnimation.setToX(isOn ? 100-50 : 0);
			fillAnimation.setFromValue(isOn? Color.WHITE : Color.LAWNGREEN);
			fillAnimation.setToValue(isOn? Color.LAWNGREEN : Color.WHITE);
			animation.play();
		});
		
		
	}
	
	public void setGraph(Graph g) {
		this.g=g;
	}
}
