package GUI;

import javafx.scene.*;
import javafx.stage.*;
import javafx.scene.paint.Color;
import javafx.application.*;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.shape.Rectangle;
import java.io.File;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import algorithms.ExportImport;
import graph.*;
import javafx.scene.layout.Pane;
import javafx.scene.layout.BorderPane;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.layout.VBox;
import javafx.scene.layout.*;
import javafx.scene.input.MouseEvent;
import graph.Node;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javax.swing.undo.*;


import UndoRedo.AddNode;

public class GraphGUI extends Application {
	
	boolean boo=true;
	double x0,y0,x1,y1;
	Rectangle rect;
	private Pane nodesANDedges = new Pane();
	private BorderPane root = new BorderPane();
	private Graph graph = null;
	private Scene scene;
	private Stage myStage;
	private MenuGUI menuGUI;
	private SideMenuGUI sideMenu;
	private LeftMenuGUI leftMenu;
	private GridPane p;
	private VBox vbox;
	private ExportImport exportImport;
	private FileChooser fcWrite;
	private UndoManager undoMan= new UndoManager();

	@Override
	public void start(Stage stage) throws Exception {
		scene = new Scene(root, 960, 640);
		myStage = stage;

		stage.setTitle("Graph Visualization");
		stage.setScene(scene);
		stage.show();
		scene.setFill(Color.WHITE);

		scene.addEventHandler(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				for (graph.Node n : Node.getSelection())
					n.setFill(n.getColor());
				Node.getSelection().clear();
				root.getChildren().remove(rect);
				boo=true;
				if(leftMenu.getBtnAdd()) {
					undoMan.addEdit(new AddNode(graph,leftMenu,event.getSceneX(),event.getSceneY()));
					String s= leftMenu.getNewNode();
					Node n= new Node(s);
					graph.addNode(n);
					n.moveXY(event.getSceneX(),event.getSceneY());
					leftMenu.setBtnAdd(false);
				}
				event.consume();
			}
		});
		
		/*
		scene.addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if(boo) {
					rect= new Rectangle();
					root.getChildren().add(rect);
					rect.setFill(null);
					rect.setStrokeWidth(2);
					rect.setStroke(Color.BLUE);
					rect.setWidth(5); rect.setHeight(5);
					rect.setVisible(true);
					rect.setX(x0=event.getX());
					rect.setY(y0=event.getY());
					boo=false;
				}
				if(event.isPrimaryButtonDown()) {
					x1=event.getSceneX(); y1=event.getSceneY();
					rect.setWidth(rect.getWidth()+x1-x0);
					rect.setHeight(rect.getHeight()+y1-y0);
					x0=x1; y0=y1;
					for(Node n: graph.getNodes().values()) {
						if(n.getCenterX()<event.getSceneX() && n.getCenterY()<event.getSceneY()) {
							if (!Node.getSelection().contains(n)) Node.addSelection(n);
							n.setFill(n.getColor().darker().darker());
						}
					}
				}
				event.consume();
			}
		});*/

		stage.setOnCloseRequest(event->{
			Alert alert = 
			        new Alert(AlertType.INFORMATION, "Do you want to save file?",
			             ButtonType.OK, 
			             ButtonType.CANCEL);
			alert.setTitle("Date format warning");
			Optional<ButtonType> result = alert.showAndWait();

			if (result.get() == ButtonType.OK){
				fcWrite= new FileChooser();
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
				fcWrite.getExtensionFilters().add(extFilter);
				File file = fcWrite.showSaveDialog(stage);
				if(file != null){
					exportImport.exporting(file);
	            }else 
	            	return;
			} else {
				 Platform.exit();
			}
		});
		
		exportImport= new ExportImport(graph);
		sideMenu = new SideMenuGUI(myStage, scene, nodesANDedges, graph, exportImport,fcWrite, undoMan);
		vbox = sideMenu.load();
		vbox.prefHeightProperty().bind(scene.heightProperty());
		root.setRight(vbox);
		
		menuGUI = new MenuGUI(stage, scene, nodesANDedges, graph, exportImport, sideMenu.getZoom(),fcWrite, undoMan);
		MenuBar menuBar = menuGUI.load();
		menuBar.prefWidthProperty().bind(scene.widthProperty());
		root.setTop(menuBar);

		leftMenu = new LeftMenuGUI(scene, myStage, graph, sideMenu.getPicker(), undoMan);
		p = leftMenu.load();
		p.prefHeightProperty().bind(scene.heightProperty());
		root.setLeft(p);

		menuGUI.getItem().setOnAction(new EventHandler<ActionEvent>() { // Open

			@Override
			public void handle(ActionEvent event) {
				Boolean i=true;
				FileChooser fileChooser = new FileChooser();
				fileChooser.setTitle("Open Resource File");
				fileChooser.getExtensionFilters().addAll(
						new ExtensionFilter("Text Files", "*.txt"),
				         new ExtensionFilter("Text files (*.gml)", "*.gml"),
				         new ExtensionFilter("Text files (*.csv)", "*.csv"));
				File file= fileChooser.showOpenDialog(stage);
				if(file != null) {
					Pattern pp = Pattern.compile("\\w+.(.*)");
					Matcher m = pp.matcher(file.getName());
					m.find();
					if(graph!=null) { 
						graph.clear();
						nodesANDedges.getChildren().clear();
						root.getChildren().clear();
						root.setTop(menuBar);
						root.setRight(vbox);
						root.setLeft(p);
					
						i=false;
						}
					if(m.group(1).contentEquals("txt")) {
						graph= exportImport.importing(file.getName());
					}
					if(m.group(1).contentEquals("csv")) {
						CSV csv = new CSV("csv",file.getName());
						graph = csv.load(); inicSchedule();
					}
					if(m.group(1).contentEquals("gml")) {
						GML gml = new GML("gml",file.getName());
						graph = gml.load(); inicSchedule();
					}
				}else return;
				nodesANDedges.getChildren().add(graph.getEdgesGr());
				nodesANDedges.getChildren().add(graph.getNodesGr());
				nodesANDedges.getChildren().add(graph.getEdgeLabel());
				nodesANDedges.getChildren().add(graph.getNodeLabel());
				root.setCenter(nodesANDedges);
				nodesANDedges.toBack();
				menuGUI.setGraph(graph);
				sideMenu.setGraph(graph);
				leftMenu.setGraph(graph);
				exportImport.setG(graph);
				if(i) {
				ZoomingPane.incInc();
				ZoomingPane.incInc();
				ZoomingPane.incInc();
				ZoomingPane.incInc();
				ZoomingPane.incInc();}
			}
		});

	}

	public void inicSchedule() {
		double size=30;
		if(graph.getNodes().size()>500) size=15;
		double endX = root.getWidth() - vbox.getWidth()-50;
		double startX = p.getWidth()+50;
		double h = root.getHeight()-100;
		for (Node n : graph.getNodes().values()) {
			double x = startX + Math.random() * (endX - startX);
			double y = 100.0 + Math.random() * h;
			n.moveXY(x, y);
			n.setSize(size);
		}
	}

}
