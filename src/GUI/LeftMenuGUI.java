package GUI;

import javafx.stage.*;
import javafx.beans.binding.Bindings;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.scene.control.Slider;
import javafx.scene.control.ChoiceBox;
import javafx.collections.FXCollections;
import java.util.LinkedList;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MoveAction;
import javax.swing.undo.UndoManager;

import UndoRedo.AddEdge;
import UndoRedo.ChangeLabel;
import UndoRedo.ChangeNodeSize;
import UndoRedo.ColorEdge;
import UndoRedo.ColorNode;
import UndoRedo.RemoveEdge;
import UndoRedo.RemoveNode;
import algorithms.*;
import graph.*;
import javafx.scene.control.ColorPicker;

public class LeftMenuGUI {
	private Button btn1;
	private Scene scene;
	private Stage stage;
	private Graph g;
	private ToggleSwitch toggle, toggle1;
	private Expansion e= new Expansion(g);
	private Contraction c= new Contraction(g);
	private ColorPicker cp;
	private TextField txt;
	private boolean added=false;
	private boolean firstColor=false;
	private Color fisrtCol;
	private UndoManager um;

	public LeftMenuGUI(Scene scene, Stage stage,Graph g, ColorPicker c, UndoManager um) {
		this.scene=scene;
		this.g=g;
		this.stage=stage;
		cp=c;
		this.um=um;
	}
	
	public GridPane load() {
		GridPane pane = new GridPane();
		pane.setVgap(30);
		pane.setStyle("-fx-background-color:#aaaaaa");
		// pane.setPrefWidth(stage.getWidth() / 4);
		firstPart(pane);
		secondPart(pane);
		thirdPart(pane);
		return pane;
	}
	
	public boolean getBtnAdd() {
		return added;
	}
	
	public void setBtnAdd(boolean b) {
		added=b;
	}
	
	public String getNewNode() {
		return txt.getText();
	}

	private void firstPart(GridPane pane) {
		GridPane gridPane = new GridPane();
		gridPane.setVgap(10);
		gridPane.setHgap(10);
		btn1 = new Button("Add Node");
		Button btn2 = new Button("Add Edge");
		Button btn3 = new Button("Remove Node");
		Button btn4 = new Button("Remove Edge");
		btn1.setPrefSize(180,30);
		btn2.setPrefSize(180,30);
		btn3.setPrefSize(180,30);
		btn4.setPrefSize(180,30);
		txt = new TextField();
		Text text1=new Text("");
		txt.setPrefSize(180,30);

		GridPane.setConstraints(btn1, 0, 0);
		GridPane.setConstraints(txt, 1, 0);
		GridPane.setConstraints(btn2, 0, 1);
		GridPane.setConstraints(btn3, 0, 2);
		GridPane.setConstraints(btn4, 1, 2);
		GridPane.setConstraints(text1, 0, 3);
		GridPane.setConstraints(text1, 1, 3);

		gridPane.getChildren().addAll(btn1, btn2, btn3, btn4, txt,text1);
		
		pane.setConstraints(gridPane, 0, 0);
		pane.getChildren().add(gridPane);
		
		
		btn1.setOnAction(t->{  //AddNode
			String s= txt.getText();
			if(txt.getText().trim().isEmpty() || g.getNodes().containsKey(s)) {
				Alert alert = new Alert(AlertType.INFORMATION);
	        	alert.setTitle("Author");
	        	alert.setHeaderText(null);
	        	alert.setContentText("Try again!");
	        	alert.initOwner(stage);
	        	alert.showAndWait();
	        	return;
			}
			added=true;
		});
		
		btn2.setOnAction(t->{  //AddEdge
			graph.Node n1=null,n2=null;
			boolean i=true;
			for(graph.Node n: graph.Node.getSelection()) {
				if(i)  n1=n; else n2=n;
				i=false;
			}
			if(g.getEdges().containsKey(new Pair(n1.ID(), n2.ID())))  return;
			if(g.getEdges().containsKey(new Pair(n2.ID(), n1.ID())))  return;
			um.addEdit(new AddEdge(g));
			String label= "Edge " + n1.ID() + "-" + n2.ID();
			g.addEdge(new Edge(n1,n2,label));
		});
		
		
		btn3.setOnAction(t->{  //RemoveNode
			um.addEdit(new RemoveNode(g));
			for(graph.Node n: graph.Node.getSelection()) {
				g.removeNode(n);
			}
		});
		
		btn4.setOnAction(t->{  //removeEdge
			graph.Node n1=null,n2=null;
			boolean i=true;
			for(graph.Node n: graph.Node.getSelection()) {
				if(i)  n1=n; else n2=n;
				i=false;
			}
			if(n1==null || n2==null) return;
			um.addEdit(new RemoveEdge(g,n1,n2));
			if(g.getEdges().containsKey(new Pair(n1.ID(),n2.ID()))) 
				g.removeEdge(g.getEdges().get(new Pair(n1.ID(),n2.ID())));
			else 
				g.removeEdge(g.getEdges().get(new Pair(n2.ID(),n1.ID())));
				
		});
		
	}
	
	private void secondPart(GridPane pane) {
		GridPane gridPane = new GridPane();
		gridPane.setVgap(10);
		gridPane.setHgap(10);
		Button btn1 = new Button("Color Node");
		Button btn2 = new Button("Color Edge");
		btn1.setPrefSize(180,30);
		btn2.setPrefSize(180,30);
		Text txt = new Text("    Size");
		txt.setFont(Font.font(24));
		Slider slider = new Slider(5, 50, 0.5);
		slider.setPrefSize(180, 30);
		

		GridPane.setConstraints(slider, 1, 0);
		GridPane.setConstraints(txt, 0, 0);
		GridPane.setConstraints(btn1, 0, 1);
		GridPane.setConstraints(btn2, 1, 1);

		gridPane.getChildren().addAll(btn1, btn2,txt, slider);
		pane.setConstraints(gridPane, 0, 1);
		pane.getChildren().add(gridPane);
		
		slider.setOnMouseClicked(t->{
			um.addEdit(new ChangeNodeSize(g,slider,txt));
			for(graph.Node n: g.getNodes().values()) {
				n.setRadius(slider.getValue());
				txt.setText("    Size  " + (int)slider.getValue());
			}
		});
		
		btn1.setOnAction(event->{ 	//ColorNode
			um.addEdit(new ColorNode(cp));
			for(graph.Node n: graph.Node.getSelection()) {
				n.setColor(cp.getValue());
				n.setFill(cp.getValue());
				n.setStroke(cp.getValue().brighter().brighter().brighter());
			}
		});
		
		btn2.setOnAction(event->{ //ColorEdge
			um.addEdit(new ColorEdge(cp,g));
			for(graph.Node n1: graph.Node.getSelection()) {
				for(graph.Node n2: graph.Node.getSelection()) {
					if(n1==n2) continue;
					Edge e=g.getEdges().get(new Pair(n1.ID(),n2.ID()));
					if(e!=null)e.setColor(cp.getValue());
					if(e!=null)e.setStroke(cp.getValue());
				}
			}
		});
	}
	
	private void thirdPart(GridPane pane) {
		GridPane gridPane = new GridPane();
		gridPane.setVgap(10);
		gridPane.setHgap(10);
		toggle= new ToggleSwitch(g);
		toggle1= new ToggleSwitch(g);
		Button play = new Button("Play");
	    play.setPrefSize(47, 30);
		Text text= new Text(" Label Edge OFF"), text1= new Text(" Label Node OFF");
		text.setFont(Font.font(18)); text1.setFont(Font.font(18));
		text.setFill(Color.BLACK);  text1.setFill(Color.BLACK); 
		text.textProperty().bind(Bindings.when(toggle.switchedOnProperty()).then(" Label Edge OFF").otherwise(" Label Edge ON"));
		text1.textProperty().bind(Bindings.when(toggle1.switchedOnProperty()).then(" Label Node OFF").otherwise(" Label Node ON"));
		ChoiceBox cb = new ChoiceBox(FXCollections.observableArrayList( "Expansion","Contraction", "Shortest path", "Format On Degree","Central"));
		TextField newLabel= new TextField(); newLabel.setPrefSize(150, 30);
		TextField sizeLabel= new TextField(); sizeLabel.setPrefSize(150, 30);
		TextField scale= new TextField(); scale.setPrefSize(150, 30);
		Button setColorLabel = new Button("Set"); setColorLabel.setPrefSize(47, 30);
		Text t1= new Text("New label:"); t1.prefWidth(150);
		Text t2= new Text("Font size:"); t2.prefWidth(150);
		cb.setPrefSize(150, 30);
		
		GridPane.setConstraints(text1, 0, 0);
		GridPane.setConstraints(toggle1, 1, 0);
		GridPane.setConstraints(text, 0, 1);
		GridPane.setConstraints(toggle, 1, 1);
		GridPane.setConstraints(t1, 0, 4);
		GridPane.setConstraints(t2, 1, 4);
		GridPane.setConstraints(newLabel, 0, 6);
		GridPane.setConstraints(sizeLabel, 1, 6);
		GridPane.setConstraints(setColorLabel, 2, 6);
		GridPane.setConstraints(cb, 0, 9);
		GridPane.setConstraints(scale, 1, 9);
		GridPane.setConstraints(play, 2, 9);
		
		gridPane.getChildren().addAll(toggle,text,cb, play, toggle1, text1,newLabel, sizeLabel, setColorLabel, scale,t1,t2);
		pane.setConstraints(gridPane, 0, 2);
		pane.getChildren().add(gridPane);
		
		toggle1.setOnMouseClicked(event->{
			toggle1.switchedOnProperty().set(!toggle1.switchedOnProperty().get());
			if(toggle1.switchedOnProperty().get()) {
				for(graph.Node e: this.g.getNodes().values())
					e.getText().setVisible(false);
				}else
				for(graph.Node e: this.g.getNodes().values())
					e.getText().setVisible(true);
		});
		
		
		toggle.setOnMouseClicked(t->{
			toggle.switchedOnProperty().set(!toggle.switchedOnProperty().get());
			if(toggle.switchedOnProperty().get()) {
				for(graph.Edge e: this.g.getEdges().values())
					e.getText().setVisible(false);
				}else
				for(graph.Edge e: this.g.getEdges().values())
					e.getText().setVisible(true);
		});
		
		setColorLabel.setOnAction(event->{
			graph.Node n1=null, n2=null;
			boolean i=true;
			um.addEdit(new ChangeLabel(g,i, n1, n2, newLabel, sizeLabel, cp));
			for(graph.Node n : graph.Node.getSelection()) {
				if(i)n1=n; else n2=n;
				i=false;
			}
			if( n2==null) {
				n1.getText().setFont(Font.font(Double.parseDouble(sizeLabel.getText())));
				n1.getText().setFill(cp.getValue());
				n1.setLabel(newLabel.getText());
			}
			else {
				g.getEdges().get(new Pair(n1.ID(), n2.ID())).setLabel(newLabel.getText());
				g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFont(Font.font(Double.parseDouble(sizeLabel.getText())));
				g.getEdges().get(new Pair(n1.ID(), n2.ID())).getText().setFill(cp.getValue());
			}
		});
		
		play.setOnAction(event->{
			switch ((String)cb.getValue()) {
			case  "Force Atlas":
				break;
			case "Expansion":
				e.startToWork(Double.parseDouble(scale.getText()));
				break;
			case "Contraction":
				c.startToWork(Double.parseDouble(scale.getText()));
				break;
			case "Shortest path":
			graph.Node n1=null, n2=null;
			boolean i=true;
			for(graph.Node n: graph.Node.getSelection()) {
				if(i)  n1=n; else n2=n;
				i=false;
			}
			LinkedList<graph.Node> list= g.shortestPath(n1, n2);
			for(graph.Node n: list)
				n.setFill(n.getColor().darker().darker());
			break;
			case "Format On Degree":
				if(firstColor==false) {
					firstColor=true; 
					fisrtCol = cp.getValue();
					return;
				}
				g.formatingOnDegree(fisrtCol, cp.getValue());
				firstColor=false;
				break;
			case "Central":
				if(firstColor==false) {
					firstColor=true; 
					fisrtCol = cp.getValue();
					return;
				}
				g.formatCentrality(fisrtCol, cp.getValue());
				firstColor=false;
				break;
			}
		});
	}
	
	public void setGraph(Graph g) {
		if(g == null)
			System.out.println("Prazan");
		this.g=g;
		toggle.setGraph(g);
		e.setGraph(g);
		c.setGraph(g);
	}
	
	
	
}
