package graph;

import java.io.*;
import java.util.regex.*;

public class CSV extends Format {

	public CSV(String label, String fileName) {
		super("csv", fileName);
	}

	@Override
	public Graph load() {
		Graph graph = new Graph();
		try {
			File file = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String strLine;
			boolean first = true;
			Node firstNode= null,n=null;
			while ((strLine = br.readLine()) != null) {
				Pattern p = Pattern.compile("(\\w+{0,1})");
				Matcher m = p.matcher(strLine);
				while (m.find()) {
					String node = m.group(1);
					if (first) {
						if(!graph.getNodes().containsKey(node))
							n= firstNode = new Node(node);
						else
							n= firstNode= graph.getNodes().get(node);
						first = false;
					} else {
						if(!graph.getNodes().containsKey(node))
							n=new Node(node);
						else
							n= graph.getNodes().get(node);
					}
					if (!graph.getNodes().containsKey(node)) {
							graph.addNode(n);
					}
					if(n.ID().compareTo(firstNode.ID())==0) continue;
					String label="Edge "+firstNode.ID()+ "-" +n.ID();
					if(!graph.getEdges().containsKey(new Pair(n.ID(),firstNode.ID())))
							graph.addEdge(new Edge(firstNode,n,label));
				} //end of one strLine
				first = true;
			}//end of file
		} catch (Exception e) {
		}
		return graph;
	}

	public static void main(String[] args) {
		CSV csv = new CSV("csv", "CSV_Lgraph_liste.csv");
		Graph g= csv.load();
		
		System.out.println(g.getNodes().size()+ "**");
		for(Node n: g.getNodes().values())
			System.out.println(n.ID() );
		
		System.out.println("****");
		for(Edge n: g.getEdges().values())
			System.out.println(n.getLabel());
	}

}
