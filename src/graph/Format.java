package graph;

public abstract class Format {
	
	private String label;
	protected String fileName;
	
	public Format(String label, String fileName) {
		super();
		this.label = label;
		this.fileName = fileName;
	}

	public String getLabel() {
		return label;
	}

	public abstract  Graph load();
}
