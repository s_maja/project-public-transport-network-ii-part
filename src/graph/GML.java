package graph;

import java.io.*;
import java.util.regex.*;

public class GML extends Format {

	public GML(String label, String fileName) {
		super("gml", fileName);
	}

	@Override
	public Graph load() {
		Graph graph = new Graph();
		try {
			File file = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String strLine;
			br.readLine();
			br.readLine();
			while ((strLine = br.readLine()) != null) {
				Pattern p1 = Pattern.compile("(.*)");
				Matcher m1 = p1.matcher(strLine);
				if(m1.find()) {  //if reading nodes
					if(m1.group(1).contentEquals("node")) {
						br.readLine(); // [
						String line= br.readLine();
						Pattern p2= Pattern.compile("^\\s*id (.*)");
						Matcher m2= p2.matcher(line);
						if(m2.find()) {
							graph.addNode(new Node(m2.group(1)));
							line= br.readLine();
							p1= Pattern.compile("^\\s*label (.*)");
							m1= p1.matcher(line);
							if(!m1.find())
								continue; //if not matching continue
							else
								graph.getNodes().get(m2.group(1)).setLabel(m1.group(1));  //if exists label set it
						}
					}//end_node
					else if(m1.group(1).contentEquals("edge")) {
						br.readLine(); //[
						String line= br.readLine();
						Pattern p2= Pattern.compile("^\\s*source (.*)");
						Matcher m2= p2.matcher(line);
						if(m2.find()) {
							String src=m2.group(1),dst="";
							line= br.readLine();
							p2= Pattern.compile("^\\s*target (.*)");
							m2= p2.matcher(line);
							if(m2.find()) {
								dst= m2.group(1);
								String label="Edge "+src+ "-" +dst;
								if(!graph.getEdges().containsKey(new Pair(dst,src)))
								graph.addEdge(new Edge(graph.getNodes().get(src), graph.getNodes().get(dst), label));
							}
							line= br.readLine();
							p2= Pattern.compile("^ *label (.*)");
							m2= p2.matcher(line);
							if(!m2.find())
								continue; //if not matching continue
							else
								graph.getEdges().get(new Pair(src,dst)).setLabel(m2.group(1));  //if exists label set it
						}	
					}// end_edge
					else
						break;	// read last ]
				}//not_match
				br.readLine(); //read ]
			}
		} catch(Exception e) {
		}

		return graph;
	}
	
	public static void main(String[] args) {
		GML gml= new GML("gml", "GML.gml");
		Graph g=gml.load();
		
		for(Node n: g.getNodes().values())
			System.out.println(n.ID() +"  " + n.getLabel());
		
		System.out.println("****");
		for(Edge n: g.getEdges().values())
			System.out.println(n.getLabel());
	}

	
}
