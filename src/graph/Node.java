package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import GUI.DragContext;
import GUI.GraphGUI;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;

public class Node extends Circle {
	static Set<Node> selection = new HashSet<Node>();

	private double x, y;
	private Color color;
	private double size;
	private Text label; // num of station or line
	private String id;
	private HashMap<Node, Edge> myEdges;
	private static double  orgSceneX, orgSceneY;


	public Node(String s) {
		super();
		id = s;
		color = Color.CORNFLOWERBLUE;
		label = new Text(ID());
		myEdges = new HashMap<Node, Edge>();
		setParametres();
	}

	private void setParametres() {
		size = 30;
		moveXY(0.0, 0.0);
		setStroke(color.brighter().brighter());
		setStrokeWidth(3);
		setFill(color);
		setRadius(size);
		label.setFill(Color.BLACK);
		label.xProperty().bind(this.centerXProperty().subtract(label.getBoundsInLocal().getWidth() / 2));
		label.yProperty().bind(this.centerYProperty().add(label.getBoundsInLocal().getHeight() / 16));
		label.setVisible(false);
		handles();
	}



	private void handles() {

		setCursor(Cursor.HAND);

		addEventFilter(MouseEvent.MOUSE_PRESSED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				orgSceneX = t.getSceneX();
				orgSceneY = t.getSceneY();

				Node c = (Node) (t.getSource());
				c.setStroke(color.brighter().brighter().brighter());
				c.setStrokeWidth(5);
				
				/**************/
				if (t.isShiftDown() && selection.contains(c)) {
					for (Node n : selection)
						n.setFill(n.getColor());
					selection.clear();
					return;
				}

				if (t.isShiftDown()) {
					for (javafx.scene.Node n : getParent().getChildrenUnmodifiable()) {
						if (!selection.contains(n))
							selection.add((Node) n);
						((Node) n).setFill(((Node)n).getColor().darker().darker());
					}
				} else if (t.isControlDown()) {
					if (selection.contains(c)) {
						if (selection.contains(c))
							selection.remove(c);
						c.setFill(color);
					} else {
						if (!selection.contains(c))
							selection.add(c);
						c.setFill(color.darker().darker());
					}
				}
				else {
					selection.add(c);
					c.setFill(color.darker().darker());
				}
				
		
				c.toFront();
				t.consume();
			}
		});

		addEventFilter(MouseEvent.MOUSE_ENTERED,new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				orgSceneX = t.getSceneX();
				orgSceneY = t.getSceneY();

				Node c = (Node) (t.getSource());
				c.setStroke(color.brighter().brighter().brighter());
				c.setStrokeWidth(5);

				c.toFront();
				t.consume();
			}
		});

		addEventFilter(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				Node c = (Node) (t.getSource());
				c.setStrokeWidth(3);
				t.consume();
			}
		});

		addEventFilter(MouseEvent.MOUSE_RELEASED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {

				Node c = (Node) (t.getSource());
				c.setStrokeWidth(3);
				t.consume();
			}
		});

		addEventFilter(MouseEvent.MOUSE_DRAGGED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent t) {
				
					double offsetX = t.getSceneX() - Node.orgSceneX;
					double offsetY = t.getSceneY() - Node.orgSceneY;

				//Node c = (Node) (t.getSource());
					for(Node n: selection) 
						n.moveXY(offsetX, offsetY);

					Node.orgSceneX= t.getSceneX();
					Node.orgSceneY= t.getSceneY();

					t.consume();
			}
		});

	}
	
	public static Set<Node> getSelection() {
		return selection;
	}
	
	public static void addSelection(Node n) {
		selection.add(n);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public double getSize() {
		return size;
	}

	public void setSize(double size) {
		this.size = size;
	}

	public String getLabel() {
		return label.getText();
	}

	public Text getText() {
		return label;
	}

	public void setText(Text label) {
		this.label = label;
	}

	public void setLabel(String label) {
		this.label.setText(label);
		this.label.xProperty().bind(this.centerXProperty().subtract(this.label.getBoundsInLocal().getWidth() / 2));
		this.label.yProperty().bind(this.centerYProperty().add(this.label.getBoundsInLocal().getHeight() / 14));
	}

	public String ID() {
		return id;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public HashMap<Node, Edge> getMyEdges() {
		return myEdges;
	}

	public void moveXY(double xx, double yy) {
		x += xx;
		y += yy;
		setCenterX(x);
		setCenterY(y);
		for (Edge n : myEdges.values())
			n.draw();
	}
	

	public void addEdge(Edge e, Node n) {
		myEdges.put(n, e);
	}

	public void removeEdges() {
		myEdges.clear(); 
	}
}
