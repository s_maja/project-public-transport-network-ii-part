package graph;

public class Quartet<T1, T2, T3, T4> {
	T1 data1;
	T2 data2;
	T3 data3;
	T4 data4;
	
	public Quartet(T1 data1, T2 data2, T3 data3, T4 data4) {
		super();
		this.data1 = data1;
		this.data2 = data2;
		this.data3 = data3;
		this.data4 = data4;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data1 == null) ? 0 : data1.hashCode());
		result = prime * result + ((data2 == null) ? 0 : data2.hashCode());
		result = prime * result + ((data3 == null) ? 0 : data3.hashCode());
		result = prime * result + ((data4 == null) ? 0 : data4.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Quartet other = (Quartet) obj;
		if (data1 == null) {
			if (other.data1 != null)
				return false;
		} else if (!data1.equals(other.data1))
			return false;
		if (data2 == null) {
			if (other.data2 != null)
				return false;
		} else if (!data2.equals(other.data2))
			return false;
		if (data3 == null) {
			if (other.data3 != null)
				return false;
		} else if (!data3.equals(other.data3))
			return false;
		if (data4 == null) {
			if (other.data4 != null)
				return false;
		} else if (!data4.equals(other.data4))
			return false;
		return true;
	}
	
	
	
}
