package graph;

import javafx.scene.shape.Line;
import javafx.scene.text.Text;
import javafx.scene.paint.Color;


public class Edge extends Line {

	private Color color;
	private Text label;
	private Node node1;
	private Node node2;

	public Edge(Node node1, Node node2, String lab) {
		super();
		this.node1 = node1;
		this.node2 = node2;
		this.label = new Text(lab);
		this.label.setFill(Color.BLACK);
		this.label.setVisible(false);
		label.xProperty().bind(this.startXProperty().add(endXProperty().subtract(startXProperty()).divide(2)));
		label.yProperty().bind(this.startYProperty().add(endYProperty().subtract(startYProperty()).divide(2)));
		color= Color.RED;
		setStroke(color);
		draw();
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setLabel(Text label) {
		this.label = label;
	}

	public String getLabel() {
		return label.getText();
	}
	
	public Text getText() {
		return label;
	}
	
	public void setText(Text t) {
		 label=t;
	}

	public Node getNode1() {
		return node1;
	}

	public Node getNode2() {
		return node2;
	}
	
	public void setLabel(String s) {
		label.setText(s);
	}
	
	public Color getColor() {
		return color;
	}

	public  void draw() {
		setStartX(node1.getX());
		setStartY(node1.getY());
		setEndX(node2.getX());
		setEndY(node2.getY());
	}
	
	public synchronized Quartet<Double, Double, Double, Double> getPostions(){
		return new Quartet(node1.getX(), node1.getY(), node2.getX(), node2.getY());
	}
}
