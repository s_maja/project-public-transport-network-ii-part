package graph;

import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import javafx.scene.paint.*;
import javafx.scene.layout.Pane;

public class Graph {
	
	Pane nodesGr= new Pane();
	Pane edgesGr= new Pane();
	Pane nodeLabel= new Pane();
	Pane edgeLabel = new Pane();
	private HashMap<String, Node> nodes;
	private HashMap<Pair<String, String>, Edge> edges;

	public Graph() {
		super();
		nodes = new HashMap<String, Node>();
		edges = new HashMap<Pair<String, String>, Edge>();
		edgeLabel.setMouseTransparent(true);
		nodeLabel.setMouseTransparent(true);
	}
	
	public Pane getNodesGr() {
		return nodesGr;
	}

	public Pane getEdgesGr() {
		return edgesGr;
	}

	public HashMap<String, Node> getNodes() {
		return nodes;
	}

	public HashMap<Pair<String, String>, Edge> getEdges() {
		return edges;
	}

	public Pane getNodeLabel() {
		return nodeLabel;
	}

	public Pane getEdgeLabel() {
		return edgeLabel;
	}

	public void setGroups(Pane n, Pane e) {
		nodesGr=n;
		edgesGr=e;
	}
	
	public  void addNode(Node n) {
		if(nodes.containsKey(n.ID())) return;
		nodes.put(n.ID(), n);
		nodesGr.getChildren().add(n);
		nodeLabel.getChildren().add(n.getText());
	}

	public  void addEdge(Edge e) {
		Pair<String, String> p = new Pair<String, String>(e.getNode1().ID(), e.getNode2().ID());
		edges.put(p, e);
		e.getNode1().addEdge(e,e.getNode2());
		e.getNode2().addEdge(e, e.getNode1());
		edgesGr.getChildren().add(e);
		edgeLabel.getChildren().add(e.getText());
	}
	
	public void removeNode(Node n) {
		if(!nodes.containsKey(n.ID())) return;
		for(Edge e: n.getMyEdges().values()) {
			edges.remove(new Pair(e.getNode1().ID(),e.getNode2().ID()));//delete all edges whit this node
			if(e.getNode1()==n) //from other nodes delete edges with current node
				e.getNode2().getMyEdges().remove(new Pair(e.getNode1().ID(),e.getNode2().ID()));
			else
				e.getNode1().getMyEdges().remove(new Pair(e.getNode1().ID(),e.getNode2().ID()));
			e.getText().setVisible(false);
			edgesGr.getChildren().remove(e);
		}
		n.removeEdges();  //delete all edges of this node
		n.getText().setVisible(false); //can I delete this
		nodes.remove(n.ID());
		nodesGr.getChildren().remove(n);
	}
	
	public void removeEdge(Edge e) {
		String e1=e.getNode1().ID();
		String e2= e.getNode2().ID();
		if(!edges.containsKey(new Pair(e.getNode1().ID(), e.getNode2().ID()))) return; 
		//from node1 and node2 delete this edge
		e.getNode1().getMyEdges().remove(e.getNode2());
		e.getNode2().getMyEdges().remove(e.getNode1());
		//remove from global map of edges
		edges.remove(new Pair(e.getNode1().ID(), e.getNode2().ID()));
		edgesGr.getChildren().remove(e);
		e.getText().setVisible(false);
	}

	
	public LinkedList<Node> shortestPath(Node n1, Node n2){
		HashMap<String, Boolean> visited= new HashMap<String, Boolean>();
		HashMap<String, String> prev= new HashMap<String, String>();
		LinkedList<Node> shortPath= new LinkedList<Node>();
		LinkedList<Node> q= new LinkedList<Node>();
		
		visited.put(n1.ID(), true);
		q.push(n1);
		while(!q.isEmpty()) {
			Node n=q.pop();
			if(n==n2) break;
			for(Edge e: n.getMyEdges().values()) {
				Node node;
				if(e.getNode1()!=n)
					node=e.getNode1();
				else
					node=e.getNode2();
				if(!visited.containsKey(node.ID())) {
					visited.put(node.ID(), true);
					prev.put(node.ID(), n.ID());
					q.push(node);
				}
			}
		}
		
		String last=n2.ID();
		shortPath.push(nodes.get(last));
		while(!last.equals(n1.ID())) {
			last= prev.get(last);
			shortPath.push(nodes.get(last));
		}
		return shortPath;
	}

	public void clear() {
		for(Node n: nodes.values())
			n.getMyEdges().clear();
		nodes.clear();
		edges.clear();
		edgeLabel.getChildren().clear();
		nodeLabel.getChildren().clear();
		nodesGr.getChildren().clear();
		edgesGr.getChildren().clear();
	}
	
	public void formatingOnDegree(Color c1, Color c2) {
		int maxDegree=0, minDegree= Integer.MAX_VALUE;
		for(Node n:  nodes.values()) {
			if(n.getMyEdges().size()>maxDegree) maxDegree=n.getMyEdges().size();
			if(n.getMyEdges().size()<minDegree) minDegree=n.getMyEdges().size();
		}
		double range= maxDegree - minDegree;
		double rangeR= c1.getRed() - c2.getRed();
		double rangeG=c1.getGreen() - c2.getGreen();
		double rangeB=c1.getBlue() - c2.getBlue();
		
		for(Node n: nodes.values()) {
			double degree=(n.getMyEdges().size()- minDegree)/range;
			
			double R=c1.getRed()+rangeR*degree;
			if(R<0.0) R=0.0; if(R>1.0) R=1.0;
		    double G=c1.getRed()+rangeG*degree;
		    if(G<0.0) G=0.0; if(G>1.0) G=1.0;
		    double B=c1.getRed()+rangeB*degree;
		    if(B<0.0) B=0.0; if(B>1.0) B=1.0;
		    
			n.setSize(10+50*degree); n.setRadius(10+50*degree);
			n.setColor(Color.color(R, G, B)); 
			n.setFill(Color.color(R,G, B));
			n.setStroke(Color.color(R,G, B).brighter().brighter().brighter());
		}
	}

	public void formatCentrality(Color c1, Color c2) {
		Node node=null;
		for(Node n : Node.getSelection()) {
			node=n;
		}
		if(node==null) return;
		double maxR=0, minR= Double.MAX_VALUE;
		for(Node n: getNodes().values()) {
			if(n==node) continue;
			double r= Math.sqrt((n.getCenterX()-node.getCenterX())*(n.getCenterX()-node.getCenterX()) + 
					(n.getCenterY()-node.getCenterY())*(n.getCenterY()-node.getCenterY()));
			if(r<minR) minR=r;
			if(r>maxR) maxR=r;
		}
		double range=maxR-minR;
		double rangeR= c1.getRed() - c2.getRed();
		double rangeG=c1.getGreen() - c2.getGreen();
		double rangeB=c1.getBlue() - c2.getBlue();
		for(Node n: nodes.values()) {
			double r= Math.sqrt((n.getCenterX()-node.getCenterX())*(n.getCenterX()-node.getCenterX()) + 
					(n.getCenterY()-node.getCenterY())*(n.getCenterY()-node.getCenterY()));
			double degree=(r- minR)/range;
			
			double R=c1.getRed()+rangeR*degree;
			if(R<0.0) R=0.0; if(R>1.0) R=1.0;
		    double G=c1.getRed()+rangeG*degree;
		    if(G<0.0) G=0.0; if(G>1.0) G=1.0;
		    double B=c1.getRed()+rangeB*degree;
		    if(B<0.0) B=0.0; if(B>1.0) B=1.0;
		    
			n.setColor(Color.color(R, G, B)); 
			n.setFill(Color.color(R,G, B));
			n.setStroke(Color.color(R,G, B).brighter().brighter().brighter());
		}
	}

}
