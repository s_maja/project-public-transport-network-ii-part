package algorithms;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import graph.*;
import javafx.scene.paint.Color;

public class ExportImport {
	Graph g;
	String fileName;
	
	public ExportImport(Graph g){
		this.g= g;
	}
	
	public void exporting(File fn) {
		try {
		FileWriter fileWriter = new FileWriter(fn);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			for(Node n:  g.getNodes().values()) {
				bufferedWriter.write("node");
				 bufferedWriter.newLine();
				bufferedWriter.write(n.ID() +";"+ n.getLabel()+";"+ n.getCenterX()+";"+ n.getCenterY()+ ";");
				String s=""+n.getSize();
				bufferedWriter.write( n.getColor().getRed() +";" + n.getColor().getGreen() +";" + n.getColor().getBlue() +";"  + s + ";");
	            bufferedWriter.newLine();
			}
		
			for(Edge e: g.getEdges().values()) {
				bufferedWriter.write("edge");
				 bufferedWriter.newLine();
				bufferedWriter.write(e.getNode1().ID() + ";"+ e.getNode2().ID()+ ";" + e.getLabel() + ";");
				bufferedWriter.write( e.getColor().getRed() +";" + e.getColor().getGreen() +";" + e.getColor().getBlue() +";");
			    bufferedWriter.newLine();
			}
		 bufferedWriter.close();
		}
		catch(Exception e) {}
	}
	
	public Graph importing(String fileName) {
		Graph graph= new Graph();
		try {
		File file = new File(fileName);
		BufferedReader br = new BufferedReader(new FileReader(file));

		String strLine;
		while ((strLine = br.readLine()) != null) {
			Pattern p = Pattern.compile("^(.*)");
			Matcher m = p.matcher(strLine);
			m.find();

			if(m.group(1).contentEquals("node")) {
				strLine= br.readLine();
				p = Pattern.compile("(.*);(.*);(.*);(.*);(.*);(.*);(.*);(.*);");
				m = p.matcher(strLine);
				if(m.find()) {
					Node n;
					graph.addNode(n=new Node(m.group(1)));
					n.setLabel(m.group(2));
					n.setX(0.0); n.setY(0.0);
					n.moveXY(Double.parseDouble(m.group(3)), Double.parseDouble(m.group(4)));
					Color c=Color.color(Double.parseDouble(m.group(5)),Double.parseDouble(m.group(6)),Double.parseDouble(m.group(7)));
					n.setFill(c);
					n.setStroke(c.brighter().brighter().brighter());
					n.setColor(c);
					n.setSize(Double.parseDouble(m.group(8)));
				}
			}
			else
			{
				strLine= br.readLine();
				p = Pattern.compile("(.*);(.*);(.*);(.*);(.*);(.*);");
				m = p.matcher(strLine);
				if(m.find()) {
				graph.addEdge(new Edge(graph.getNodes().get(m.group(1)), graph.getNodes().get(m.group(2)), m.group(3)));
				if(graph.getEdges().containsKey(new Pair(m.group(1),m.group(2))))
				graph.getEdges().get(new Pair(m.group(1),m.group(2))).draw();
				Color c=Color.color(Double.parseDouble(m.group(4)),Double.parseDouble(m.group(5)),Double.parseDouble(m.group(6)));
				graph.getEdges().get(new Pair(m.group(1),m.group(2))).setColor(c);
				graph.getEdges().get(new Pair(m.group(1),m.group(2))).setStroke(c);
				}
			}
			}
		}catch(Exception e) { System.out.println("aa");}
		return graph;
	}

	public void setG(Graph g) {
		this.g = g;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}
