package algorithms;

import java.lang.management.PlatformLoggingMXBean;

import graph.*;
import javafx.application.Platform;

public class Expansion extends Thread {
	private Graph g;
	private boolean working = false;
	private double scaleFactor;
	private double 	 xx,yy;
	public Expansion(Graph g) {
		this.g = g;
		scaleFactor = 1.2;
		setDaemon(true);
		start();
	}

	public void run() {
		try {
			while (!interrupted()) {
				synchronized (this) {
					while (!working)
						wait();
				}
				double minX = Double.MAX_VALUE, maxX = 0.0;
				double maxY = 0.0, minY = Double.MAX_VALUE;
				for (Node n : g.getNodes().values()) {
					if (n.getCenterX() < minX) minX = n.getCenterX();
					if (n.getCenterY() < minY) minY = n.getCenterY();
					if (n.getCenterX() > maxX) maxX = n.getCenterX();
					if (n.getCenterY() > maxY) maxY = n.getCenterY();
				}
			
				double x=(minX+maxX)/2; double y=(minY+maxY)/2;
				for(Node n: g.getNodes().values()) {
					Platform.runLater(new Runnable(){
						public void run() {
							n.moveXY(-(x-n.getCenterX())*scaleFactor,-(y-n.getCenterY())*scaleFactor);
						}
					});
				}
				working = false;
			}
		} catch (InterruptedException e) {
		}
	}

	public synchronized void startToWork(double scale) {
		scaleFactor=scale;
		working = true;
		notify();
	}

	public void stopToWork() {
		working = false;
	}

	public void interruptThread() {
		interrupt();
	}

	public void setScaleFactor(double i) {
		scaleFactor = i;
	}
	
	public void setGraph(Graph gg) {
		g=gg;
	}
}
